export const buildDataId = (id: any) => {
    return `[data-id=${id}]`;
};
export const buildAutomationId = (id: string) => {
    return `[data-automationid=${id}]`;
};
export const buildDataItem = (id: string) => {
    return `[data-item=${id}]`;
};
export const buildFormControlName = (id: string) => {
    return `[formcontrolname=${id}]`;
};
