declare namespace Cypress {
    interface Chainable<Subject> {
        login(username: string, password: string): Chainable<Subject>;

        getDataId(
            selector: string,
            options?: Partial<Loggable & Timeoutable & Withinable & Shadow>
        ): Chainable<Subject>;
        getAutomationId(
            selector: string,
            options?: Partial<Loggable & Timeoutable & Withinable & Shadow>
        ): Chainable<Subject>;
        getDataItem(
            selector: string,
            options?: Partial<Loggable & Timeoutable & Withinable & Shadow>
        ): Chainable<Subject>;
        getFormControlName(
            selector: string,
            options?: Partial<Loggable & Timeoutable & Withinable & Shadow>
        ): Chainable<Subject>;

        findDataId(selector: string, options?: Partial<Loggable & Timeoutable & Shadow>): Chainable<Subject>;
        findAutomationId(selector: string, options?: Partial<Loggable & Timeoutable & Shadow>): Chainable<Subject>;
        findDataItem(selector: string, options?: Partial<Loggable & Timeoutable & Shadow>): Chainable<Subject>;
        findFormControlName(selector: string, options?: Partial<Loggable & Timeoutable & Shadow>): Chainable<Subject>;
    }
}
