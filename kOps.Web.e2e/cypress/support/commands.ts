// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

import { buildAutomationId, buildDataId, buildDataItem, buildFormControlName } from './utils';

Cypress.Commands.add('login', (username, password) => {
    cy.request({
        method: 'POST',
        url: Cypress.env('apiUrl') + '/Account/Signin',
        body: {
            Email: username,
            Password: password,
        },
    }).then(resp => {
        const accountJson = JSON.stringify(resp.body);
        localStorage.setItem('curr_user', accountJson);
    });

    cy.request({
        method: 'GET',
        url: Cypress.env('apiUrl') + '/AuthorizationServer/default',
    }).then(resp => {
        const smartuseServer = JSON.stringify({
            connection: { server: resp.body },
        });
        localStorage.setItem('smartuse.cookies', smartuseServer);
    });
});

Cypress.Commands.add('getDataId', (selector, options) => cy.get(buildDataId(selector), options));
Cypress.Commands.add('getAutomationId', (selector, options) => cy.get(buildAutomationId(selector), options));
Cypress.Commands.add('getDataItem', (selector, options) => cy.get(buildDataItem(selector), options));
Cypress.Commands.add('getFormControlName', (selector, options) => cy.get(buildFormControlName(selector), options));

Cypress.Commands.add('findDataId', { prevSubject: true }, (subject: any, selector, options) =>
    subject.find(buildDataId(selector), options)
);
Cypress.Commands.add('findAutomationId', { prevSubject: true }, (subject: any, selector, options) =>
    subject.find(buildAutomationId(selector), options)
);
Cypress.Commands.add('findDataItem', { prevSubject: true }, (subject: any, selector, options) =>
    subject.find(buildDataItem(selector), options)
);
Cypress.Commands.add('findFormControlName', { prevSubject: true }, (subject: any, selector, options) =>
    subject.find(buildFormControlName(selector), options)
);
