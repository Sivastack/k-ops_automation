import { Then, When } from 'cypress-cucumber-preprocessor/steps';

When('I navigate to website', () => {
    const webUrl = Cypress.env('webUrl');
    cy.visit(webUrl);
});

Then(/^Success toast is displayed$/, () => {
    cy.get('.p-toast-message-success').should('be.visible');
});

When('I change organization {string}', organizationName => {
    cy.get('su-user-menu').click();
    cy.get('.user-menu').find('[data-automationid=changeOrganization]').click();
    selectOrganization(organizationName);
});

function selectOrganization(organizationName: string) {
    cy.get('su-select-organization').find('button').contains(organizationName).click();
}
