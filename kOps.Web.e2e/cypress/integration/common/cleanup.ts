import { Given } from 'cypress-cucumber-preprocessor/steps';

Given(/^Project "([^"]*)" does not exist$/, async projectName => {
    const projectEndpoint = Cypress.env('apiUrl') + '/Project';

    cy.request('GET', projectEndpoint)
        .its('body')
        .then((projects: any[]) => {
            const projectId = projects.find(p => p.name === projectName)?.id;
            if (projectId) {
                cy.request('DELETE', projectEndpoint + '/' + projectId);
            }
        });
});
