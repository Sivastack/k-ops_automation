Feature: Test the feature of project team tab

    Scenario: Add the project manager
        Given user login into application
        When user launches sample project
        And user navigates to project team tab
        And user adds a manager to the project
        Then validates new manager has been created

    Scenario: Add the collabrator
        Given user login into application
        When user launches sample project
        And user navigates to project team tab
        And user clicks on collaborator submenu
        And user adds the collaborator to the project
        Then validates new collaborator has been created

