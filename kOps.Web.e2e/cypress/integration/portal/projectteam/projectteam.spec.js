import { Then, When } from 'cypress-cucumber-preprocessor/steps';

beforeEach(() => {
    Cypress.session.clearAllSavedSessions()
    cy.clearCookies()
    cy.clearLocalStorage()
    indexedDB.deleteDatabase('firebaseLocalStorageDb');
    cy.visit(Cypress.env('webUrl'));
});


When('user login into application', () => {
    cy.get("#button-1021").should('be.visible').click();
    cy.get('#email').type("steja@stackct.com");
    cy.get('#password').type("Teja123!");
    cy.get("#btn-login").should('be.enabled').click();
    cy.contains('Sample Project').should('be.visible')
})

When('user launches sample project', () => {
    cy.contains('Sample Project').should('be.visible').click()
    cy.contains('Launch Sample Project').should('be.visible').click()
    cy.contains('a', 'Project Admin').should('be.visible')
})

When('user navigates to project team tab', () => {
    cy.contains('a', 'Project Team').should('be.visible').click()
    cy.contains('Managers').should('be.visible')
})

When('user adds a manager to the project', () => {
    cy.contains('INVITE').should('be.visible').click()
    cy.get('input[placeholder="name@example.com"]').should('be.visible').type('manager51941@gmail.com')
    cy.get('input[placeholder="First Name (optional)"]').should('be.visible').type('abcd')
    cy.wait(3000)

    cy.get('span.fa-chevron-right').should('be.visible').click()
    cy.contains('Send').should('be.visible').click()
    cy.contains('Done').should('be.visible').click()
})

Then('validates new manager has been created', () => {
    cy.contains('a', 'manager51941@gmail.com').should('be.visible')
})

When('user adds the collaborator to the project', () => {
    cy.contains('INVITE').should('be.visible').click()
    cy.get('input[placeholder="name@example.com"]').should('be.visible').type('collobrator319001@gmail.com')
    cy.get('input[placeholder="First Name (optional)"]').should('be.visible').type('abcd')
    cy.wait(3000)
    cy.get('span.fa-chevron-right').should('be.visible').click()
    cy.contains('Send').should('be.visible').click()
    cy.contains('Done').should('be.visible').click()
})

Then('validates new collaborator has been created', () => {
    cy.contains('a', 'collobrator319001@gmail.com').should('be.visible')
})

When('user clicks on collaborator submenu', () => {
    cy.contains('Collaborators').should('be.visible').click()
    cy.contains('All collaborators').should('be.visible')
    cy.wait(3000)
})