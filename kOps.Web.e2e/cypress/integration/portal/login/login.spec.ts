import { Then, When } from 'cypress-cucumber-preprocessor/steps';

beforeEach(() => {
    Cypress.session.clearAllSavedSessions()
    cy.clearCookies()
    cy.clearLocalStorage()
    indexedDB.deleteDatabase('firebaseLocalStorageDb');
    cy.visit(Cypress.env('webUrl'));
});

afterEach(() => {
    cy.clearCookies()
    Cypress.session.clearAllSavedSessions()
});

When('I enter email {string}', (username: string) => {
    cy.get('#email').type(username);
});

When('I click on login button', () => {
    cy.log("test log")
    // cy.request("#m/dashboard")
    cy.get("#button-1021").should('be.visible').click();
    //  window.stop();
    cy.log("clicked on element")
});

When('I click on login', () => {
    cy.get("#btn-login").should('be.enabled').click();
});

When('I click next', () => {
    cy.get("button[type='submit']").click();
});

When('I enter password {string}', password => {
    cy.get('#password').type(password);
});

Then('validate user logged into application succesfully', () => {
    cy.contains('Sample Project').should('be.visible')
})